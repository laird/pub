install
lang en_GB.UTF-8
keyboard us
timezone US/Pacific
auth --useshadow --enablemd5
selinux --disabled
firewall --disabled
services --enabled=sshd,network --disabled=NetworkManager
eula --agreed
reboot
text
# provision storage
bootloader --location=mbr --append="console=tty console=ttyS0 notsc"
%include /tmp/storage.ks
# provision network
%include /tmp/network.ks
skipx

rootpw --iscrypted $1$y0xUZV8E$RzDS8jTO9iKtCcSvnH6bv0

url --url="http://mirrors.cat.pdx.edu/centos/7.3.1611/os/x86_64/"
repo --name=base --baseurl=http://mirrors.cat.pdx.edu/centos/7.3.1611/os/x86_64/
repo --name=updates --baseurl=http://mirrors.cat.pdx.edu/centos/7.3.1611/updates/x86_64/

%packages --nobase --ignoremissing
@core --nodefaults
-aic94xx-firmware*
-alsa-*
-iwl*firmware
-NetworkManager*
-iprutils
%end

## PRE
%pre --log=/tmp/pre.log
# parse command line options and set variables
set -- `cat /proc/cmdline`
for I in $*; do case "$I" in *=*) eval $I;; esac; done
# set hostname=
if [ $hostname ]; then
    echo "network --bootproto=dhcp --onboot=on --noipv6 --hostname=$hostname" > /tmp/network.ks
else
    echo "network --bootproto=dhcp --onboot=on --noipv6" > /tmp/network.ks
fi
if [ $ksip ]; then
    echo "network --bootproto=static --onboot=on --noipv6 --hostname=$ksfqdn --ip=$ksip --netmask=255.255.255.0 --gateway=10.100.10.5 --nameserver=10.100.10.5 --device=link" > /tmp/network.ks
fi
if [[ $disk ]]; then
  wget http://bitbucket.org/laird/pub/raw/master/ks/disk-$disk.ks -O /tmp/disk.ks
  if [[ $? -ne 0 ]]; then
      wget http://bitbucket.org/laird/pub/raw/master/ks/disk-single.ks -O /tmp/disk.ks
  fi
  source /tmp/disk.ks
else
  # no disk argument, determine the number of drives and names
  set $(list-harddrives)
  let numd=$#/2
  ROOT=$1
  if [ $numd == 5 ]; then
    # create /tmp/storage.ks
    echo "zerombr" > /tmp/storage.ks
    echo "clearpart --all --initlabel" >> /tmp/storage.ks
    echo "part /boot --size=4096 --ondisk=$1" >> /tmp/storage.ks
    echo "part pv.11 --size=1024 --grow --ondisk=$1" >> /tmp/storage.ks
    echo "part pv.12 --size=1024 --grow --ondisk=$3" >> /tmp/storage.ks
    echo "part pv.13 --size=1024 --grow --ondisk=$5" >> /tmp/storage.ks
    echo "part pv.14 --size=1024 --grow --ondisk=$7" >> /tmp/storage.ks
    echo "part pv.15 --size=1024 --grow --ondisk=$9" >> /tmp/storage.ks
    echo "volgroup vg pv.11 pv.12 pv.13 pv.14 pv.15" >> /tmp/storage.ks
    echo "logvol swap --name=swap_lv --fstype=swap --vgname=vg --size=5120" >> /tmp/storage.ks
    echo "logvol / --name=root_lv --fstype=xfs --vgname=vg --size=1 --grow" >> /tmp/storage.ks
    #
  else
    # create /tmp/storage.ks
    echo "ignoredisk --only-use=$ROOT" > /tmp/storage.ks
    echo "zerombr" >> /tmp/storage.ks
    echo "clearpart --all --initlabel" >> /tmp/storage.ks
    echo "part swap --asprimary --fstype="swap" --size=1024" >> /tmp/storage.ks
    echo "part / --fstype xfs --size=200 --grow" >> /tmp/storage.ks
  fi
fi
%end

## POST
%post --log /root/post.log
echo --------- pre log ---------
cat >> /root/precap.log << "EOF"
%include /tmp/pre.log
EOF
echo --------- post log ---------
%include /tmp/storage.ks

%include https://bitbucket.org/laird/pub/raw/master/ks/homelab.ks

%end
