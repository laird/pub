lang en_US.UTF-8
keyboard us
timezone US/Arizona --ntpservers time.cloudflare.com
#auth --useshadow --enablemd5
authselect select minimal without-nullok with-pamaccess
selinux --disabled
firewall --disabled
services --enabled=NetworkManager,sshd
eula --agreed
reboot
#text

# provision storage
bootloader --location=mbr --append="net.ifnames=0 console=tty console=ttyS0 notsc"
#bootloader --timeout 1 --append "console=tty0 console=ttyS0,115200 net.ifnames.prefix=net ipv6.disable=0 quiet systemd.show_status=yes"
%include /tmp/storage.ks
# provision network
%include /tmp/network.ks
skipx

rootpw --iscrypted $1$y0xUZV8E$RzDS8jTO9iKtCcSvnH6bv0

url --url http://phx.mirrors.clouvider.net/almalinux/9.2/BaseOS/x86_64/os/
repo --name=centos-extras --baseurl=http://phx.mirrors.clouvider.net/almalinux/9.2/extras/x86_64/os/
repo --name=centos-appstream --baseurl=http://phx.mirrors.clouvider.net/almalinux/9.2/AppStream/x86_64/os/

%packages --ignoremissing --instLangs en_US
@^minimal-environment
# --excludedocs
# --excludeWeakdeps
# --ignoremissing
# --nocore
bash-completion
bind-utils
glibc-minimal-langpack
#vim-enhanced
yum-utils
epel-release
%end

# PRE
#
%pre --log=/tmp/pre.log
# parse command line options and set variables 
set -- `cat /proc/cmdline`
for I in $*; do case "$I" in *=*) eval $I;; esac; done

if [ -n $vnc ]; then
  #inst.vnc
  inst.text
else
  inst.text
fi

# set hostname=
if [ $hostname ]; then
	echo "network --bootproto=dhcp --onboot=on --noipv6 --hostname=$hostname" > /tmp/network.ks
else
	echo "network --bootproto=dhcp --onboot=on --noipv6" > /tmp/network.ks
fi
if [ $ksip ]; then
    subnet=${ksip%.*}
    echo "network --bootproto=static --onboot=on --noipv6 --hostname=$ksfqdn --ip=$ksip --netmask=255.255.255.0 --gateway=${subnet}.1 --nameserver=${subnet}.1 --device=link" > /tmp/network.ks
    echo "# subnet = $subnet" >> /tmp/network.ks
fi
# 
# storage provisioning
#
if [[ $disk ]]; then
  wget http://bitbucket.org/laird/pub/raw/master/ks/disk-$disk.ks -O /tmp/storage.ks
  if [[ $? -ne 0 ]]; then
  	wget http://bitbucket.org/laird/pub/raw/master/ks/disk-single.ks -O /tmp/storage.ks
  fi
  source /tmp/storage.ks
else
  # no disk argument, determine the number of drives and names
  set $(list-harddrives)
  let numd=$#/2
  # special cases
  if [ $numd == 5 ]; then
    wget http://bitbucket.org/laird/pub/raw/master/ks/disk-5.ks -O /tmp/disk.ks
    break
  fi

  # pick the first drive that is not removable and is over MINSIZE
  DIR="/sys/block"
  # minimum and maximum size of hard drive needed specified in GIGABYTES
  MINSIZE=10
  MAXSIZE=1200
  # The loop first checks NVME then SATA/SAS drives:
  for d in $DIR/sd* $DIR/nvme* 
  do
    DEV=`basename "$d"`
    if [ -d $DIR/$DEV ]; then
      # Note: the removable file may have an incorrect value:
      # supermicro systems reports 1 for all drives no matter settings
      #if [[ "`cat $DIR/$DEV/removable`" = "0" ]]
      #then
        # /sys/block/*/size is in 512 byte chunks
        GB=$((`cat $DIR/$DEV/size`/2**21))
        echo "Disk device $DEV has size $GB GB"
        if [ $GB -gt $MINSIZE -a $GB -lt $MAXSIZE -a -z "$ROOTDRIVE" ]
        then
          ROOTDRIVE=$DEV
          echo "Select ROOTDRIVE=$ROOTDRIVE"
        fi
      #fi
    fi
  done
  echo "starting config of $ROOTDRIVE" >/dev/tty1
  # create /tmp/storage.ks
  echo "zerombr" > /tmp/storage.ks
  echo "clearpart --drives=$ROOTDRIVE --all --initlabel" >> /tmp/storage.ks
  echo "ignoredisk --only-use=$ROOTDRIVE" >> /tmp/storage.ks
  echo "reqpart --add-boot" >> /tmp/storage.ks
  echo "part swap --size 32768 --asprimary" >> /tmp/storage.ks
  echo "part pv.01 --fstype xfs --size=1 --grow --asprimary" >> /tmp/storage.ks
  echo "volgroup VolGroup00 pv.01" >> /tmp/storage.ks
  echo "logvol / --fstype xfs --name=lv_root --vgname=VolGroup00 --size=32768 --grow" >> /tmp/storage.ks
fi 

%end

# POST
#
%post --log /root/post.log
echo --------- pre log ---------
cat >> /root/pre.log << "EOF"
%include /tmp/pre.log
echo --------- storage.ks ---------
%include /tmp/storage.ks
echo --------- network.ks ---------
%include /tmp/network.ks
EOF
echo --------- post log ---------
echo --------- setup homelab firstboot ---------
#%include http://phx.mirrors.clouvider.net/ks/homelab.ks
#wget http://bitbucket.org/laird/pub/raw/master/ks/firstboot -O /root/firstboot
curl -o /root/firstboot https://bitbucket.org/laird/pub/raw/master/ks/firstboot
chmod 755 /root/firstboot
cat >> /etc/systemd/system/firstboot.service << "EOF"
[Unit]
Description=boot time fun

[Service]
Type=oneshot
ExecStart= /root/firstboot

[Install]
WantedBy=multi-user.target
EOF
systemctl enable firstboot

%end
