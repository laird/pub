#
#----- partitioning logic ------
#
zerombr
clearpart --all --drives=vda
#bootloader --location=mbr --driveorder=$ROOTDRIVE
part swap --recommended --ondisk=vda
part / --size=4096 --grow --ondisk=vda

#reqpart --add-boot
#part swap --size 32768 --asprimary
#part pv.01 --fstype xfs --size=1 --grow --asprimary
#volgroup VolGroup00 pv.01
#logvol / --fstype xfs --name=lv_root --vgname=VolGroup00 --size=32768 --grow
