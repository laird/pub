#
#----- partitioning logic ------
#
# this script creates /tmp/part-include
# %include /tmp/part-include
#
#
#chvt 2
#echo "Detecting and partitioning storage"
#echo "---------------------------------------------------"
#sleep 15
#chvt 1
#
# pick the first drive that is not removable and is over MINSIZE
# (this is needed for 6.4 forward due to the install media sometimes
# being presented lower than the target install disk)
DIR="/sys/block"
# minimum size of hard drive needed specified in GIGABYTES
MINSIZE=6
ROOTDRIVE=""
ROOTSIZE=0
# /sys/block/*/size is in 512 byte chunks
for DEV in sda sdb sdc sdd hda hdb; do
  if [ -d $DIR/$DEV ]; then
    REMOVABLE=`cat $DIR/$DEV/removable`
    if (( $REMOVABLE == 0 )); then
      SIZE=`cat $DIR/$DEV/size`
      GB=$(($SIZE/2**21))
      if [ $GB -gt $MINSIZE ]; then
        echo "# drive $DEV is $(($SIZE/2**21)) GB" > /tmp/part-include
        if [ -z $ROOTDRIVE ]; then
          ROOTDRIVE=$DEV
		  ROOTSIZE=$GB
        fi
      fi
    fi
  fi
done
# Determine amount of memory in order to setup sane swap...
MEM_SIZE_KB=`cat /proc/meminfo | grep MemTotal | awk '{print $2}'`
MEM_SIZE_MB=`awk -v MSZKB=$MEM_SIZE_KB 'BEGIN{printf("%.2f\n",((MSZKB)/1024))}' | awk '{printf("%d\n",$1+0.5)}'`
MEM_SIZE_GB=`awk -v MSZKB=$MEM_SIZE_KB 'BEGIN{printf("%.2f\n",((MSZKB)/1024/1024))}' | awk '{printf("%d\n",$1+0.5)}'`
SWAP_REC=`expr $MEM_SIZE_MB / 2`
# create the /tmp/part-include file to be sourced
cat << EOF >> /tmp/part-include
zerombr
clearpart --all --drives=$ROOTDRIVE --initlabel
bootloader --location=mbr --driveorder=$ROOTDRIVE
EOF
# Note that `part swap --recommended` is broken in weird ways, so...
if [ $ROOTSIZE -lt 128 ]; then
  # Small(ish) root drive, check to make sure we're not making a huge swap
  if [ $MEM_SIZE_GB -ge 16 ]; then
  	# We have a small(ish) drive and 16G or more of memory, cap swap at 8G 
    echo "part swap --size 8192 --ondisk=$ROOTDRIVE" >> /tmp/part-include
  else
    # Small(ish) drive and smallish memory. The right thing will happen
    echo "part swap --recommended --ondisk=$ROOTDRIVE" >> /tmp/part-include
  fi
else
  # Big drive, if we have a lot of memory work around a bug in --recommended
  if [ $MEM_SIZE_GB -gt 64 ]; then
    # Big drive and more than 64G memory, set swap to MEM_SIZE_MB / 2
    # otherwise it defaults to 4G
    echo "part swap --size $SWAP_REC --ondisk=$ROOTDRIVE" >> /tmp/part-include
  else
    # Big drive, less than 64G of memory. The right thing will happen
    echo "part swap --recommended --ondisk=$ROOTDRIVE" >> /tmp/part-include
  fi
fi
if [ $ROOTSIZE -gt 2048 ]; then
  # large / disks need a /boot
  echo "part /boot --size=500 --ondisk=$ROOTDRIVE" >> /tmp/part-include
  echo "part / --size=4096 --grow --ondisk=$ROOTDRIVE" >> /tmp/part-include
else
  echo "part / --size=4096 --grow --ondisk=$ROOTDRIVE" >> /tmp/part-include
fi
# for steps that occur in %post
cat << EOF > /tmp/part-include-post
EOF
